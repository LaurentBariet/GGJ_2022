/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_AMBIANCE = 3174479978U;
        static const AkUniqueID PLAY_EVENT_TEST = 1915892769U;
        static const AkUniqueID PLAY_MUSIQUE_INTRO = 1305709588U;
        static const AkUniqueID PLAY_PAUSE_ALL = 770128916U;
        static const AkUniqueID PLAY_RESUME_ALL = 2633259259U;
        static const AkUniqueID PLAY_SFX_CURSED_VIBES = 4220603474U;
        static const AkUniqueID PLAY_SFX_FIRE_LEVEL_01 = 3184990085U;
        static const AkUniqueID PLAY_SFX_FIRE_LEVEL_02 = 3184990086U;
        static const AkUniqueID PLAY_SFX_FIRE_LEVEL_03 = 3184990087U;
        static const AkUniqueID PLAY_SFX_FIRE_LEVEL_04 = 3184990080U;
        static const AkUniqueID PLAY_SFX_FIRE_MYSTIC_LOW_BED = 3761904413U;
        static const AkUniqueID PLAY_SFX_FIRE_TRANSITION = 3600031324U;
        static const AkUniqueID PLAY_SFX_TOCTOC_BINAURAL_01 = 137879735U;
        static const AkUniqueID PLAY_SFX_TOCTOC_BINAURAL_02 = 137879732U;
        static const AkUniqueID PLAY_SFX_TOCTOC_BINAURAL_03 = 137879733U;
        static const AkUniqueID STOP_AMBIANCE = 2839405212U;
        static const AkUniqueID STOP_MUSIQUE_INTRO = 3526698682U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID SOUNDBANK = 1661994096U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
