using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractiveObject : MonoBehaviour
{
    public EventManager eventManager;
    public String prompt;
    public UnityEvent scriptToExecute;
    public bool disableAfterUse = true;

    public void Test()
    {
        print("interaction test");
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
